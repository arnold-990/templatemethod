﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsA : MonoBehaviour
{
    OverridenA overA = new OverridenA();
    OverridenB overB = new OverridenB();
    
    public void Start()
    {
        A();
        B();
    }
    public void A()
    {
        string j;
        j = overA.TemplateMethod("Me", 1, 2, 3).ToString();
        Debug.Log(j);
    }
    public void B()
    {
        string k;
        k = overB.TemplateMethod("You", 4, 5, 6).ToString();
        Debug.Log(k);
    }
}